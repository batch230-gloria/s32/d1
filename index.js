let http = require("http");

http.createServer(function(request, response){

	if(request.url == '/items' && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		// Ends the response process
		response.end('Data retrieved from database');
	}

	if(request.url == '/items' && request.method == "POST"){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Data to be sent to the database');
	}

}).listen(4000);

console.log("Server is runnung at localhost: 4000");